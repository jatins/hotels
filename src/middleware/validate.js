/** 
 * middleware function to perform any validations.
 */
module.exports = function (req, res, next) {
	if(!req.query.api_key) {
		res.status(403).send({ok: 0, err: {code: "NoAPIKeyProvided"}})
		return;
	}

	next()
}