const TokenLimiter = require('./TokenLimiter')
const log = require('bunyan').createLogger({name: "RateLimitter"})
const mapValues = require('lodash.mapvalues');

/**
 * @param {string} - key on the basis of which to throttle requests
 * @param {defaultRate} - default rate (allowed requests/second)
 * @param {object} - key-value pair where each value signifies allowed rate for key
 * @param {number} suspendPeriod - period to suspend calls after limit exceeds (in seconds)
 */
function RateLimiter({key, defaultRate, rates, suspendPeriod}) {
	var cache = {};

	defaultRate = convertRate(defaultRate)
	rates = mapValues(rates, convertRate)

	log.info('Rate Limiter initialized with defaults:',  
		`${defaultRate.count} request(s) allowed per ${defaultRate.period} seconds per ${key}`)
	
	return function(req, res, next) {
		const value = req.query[key]

		// each unique value of 'key' gets a TokenLimiter
		if(!cache[value]) {
			const rate = rates[value] || defaultRate
			cache[value] = new TokenLimiter(rate.count, rate.period, suspendPeriod);
		}

		try {
			const limiter = cache[value]
			limiter.check()
		} catch (err) {
			if(err.code === 'LimitExceeded') {
				log.info({key, value}, 'Limit exceeded')
				res.status(403).send({ok:0, err: err})
				return;
			}
		}

		next();
	}
}

/**
 * @param  {number} - number representing rate (requests/second)
 * @return {object} - object with keys: 'count' and 'interval' (in seconds) such that rate = count/interval
 */
function convertRate(rate) {
	let count = rate;
	let period = 1;

	// example: a rate of 0.5 => 5 requests, every 10 seconds
	// a rate of 0.3 => 3 request every 10 secons
	if(!isInt(rate)) { 
		period = 10;
		count = Math.floor(rate*10)
	}

	return {count, period}
}

function isInt(num) {
	if(parseInt(num, 10) === num)
		return true

	return false
}

module.exports = RateLimiter;