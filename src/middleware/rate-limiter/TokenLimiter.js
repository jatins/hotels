const LimitExceededError = require('./LimitExceededError')

const SUSPEND_PERIOD = 5;

function init(requests, interval, suspendPeriod) {
	this.requests = requests
	this.interval = interval
	this.availableTokens = requests
	this.lastCheck = Date.now();
	this.suspended = false
	this.suspendPeriod = suspendPeriod || SUSPEND_PERIOD;
}

/**
 * A class implementing the Token Bucket approach for rate limiting
 */
class TokenLimiter {

	/**
	 * @param  {number} number of requets allowed in 'interval' seconds
	 * @param  {number} interval for which 'requests' number of requests are allowed
	 * rate = requests/interval
	 */
	constructor(requests, interval, suspendPeriod) {
		init.call(this, requests, interval, suspendPeriod)
	}

	_unsuspend() {
		init.call(this, this.requests, this.interval, this.suspendPeriod)
	}

	/**
	 * Check if the rate has exceeded. Throws 'LimitExceeded' error in case of exceeded limit
	 */
	check() {
		if(this.suspended) {
			throw new LimitExceededError();
		}

		const now = Date.now()
		const rate = this.requests/this.interval
		const timeElapsed = (now - this.lastCheck)/1000;

		this.lastCheck = now
		this.availableTokens = Math.min(this.requests, this.availableTokens + timeElapsed*rate)
		this.availableTokens = Math.floor(this.availableTokens)

		var self = this;
		if (this.availableTokens < 1) {
			this.suspended = true
			setTimeout(self._unsuspend.bind(self), self.suspendPeriod*1000)
			throw new LimitExceededError()
		} else {
			this.availableTokens--;
		}
		
	}	
}

module.exports = TokenLimiter;