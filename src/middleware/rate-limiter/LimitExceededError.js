class LimitExceededError extends Error {
	constructor() {
		super();
		this.code = 'LimitExceeded';
		this.message = 'Rate Limit Exceeded';
	}
}

module.exports = LimitExceededError