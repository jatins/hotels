const bunyan = require('bunyan');
const log = bunyan.createLogger({name: "Hotels"});

module.exports = log;