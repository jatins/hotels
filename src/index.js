const express = require('express');
const app = express();
const log = require('./logger').child({module: 'index'});
const findRoot = require('find-root');
const cwd = require('cwd')
const rootPath = findRoot(cwd())
const fs = require('fs')

const rateConfig = require('./config').rateLimitConfig;

// load middlewares
const rateLimitter = require('./middleware/rate-limiter')
const validate = require('./middleware/validate')

// converts array with item [key: 'abc', value: 123] to object {abc: 123}
function modifyRates(rates, key) {
	const overrides = {}

	rates.forEach(function(el) {
		const k = el[key];
		const v = el.value

		overrides[k] = v
	})

	return overrides
}

// rate limit on the basis of 'api_key' in query params to a 'default' rate
const {key, defaultRate, suspendPeriod, rates} = rateConfig
const limiter = rateLimitter({key, defaultRate, rates: modifyRates(rates, key), suspendPeriod})

// use the rate limitter for all '/hotels' routes
app.use('/hotels', validate, limiter, require('./hotels'))

// render README when hitting '/' path
let cachedResponse;
app.get('/', (req, res) => {
	res.type('text/plain')

	if(cachedResponse) {
		res.send(cachedResponse)
		return;
	}

	fs.readFile(rootPath + '/README.md', function (err, data) {
		if(err) {
			res.send({ok: 0, err: err})
		} else {
			cachedResponse = data.toString();
			res.send(cachedResponse)
		}
	})
})



// start litening for requests
const PORT = 3000;
app.listen(PORT, function () {
  log.info(`Listening on port ${PORT}...`);
});

