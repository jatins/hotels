/**
 * This module reads from the hotels database
 */

const fs = require('fs')
const findRoot = require('find-root')
const cwd = require('cwd')
const rootPath = findRoot(cwd())
const sortBy = require('lodash.sortby')

function getData() {
	const file = fs.readFileSync(rootPath + '/resources/hoteldb.csv').toString();
	const rows = file.split('\r')

	const hotels = []
	rows.forEach(function (row, i) {
		if(i == 0) return;

		const data = row.split(',')
		hotels.push({
			city: data[0],
			hotelId: Number(data[1]),
			room: data[2],
			price: Number(data[3])
		})
	})

	return hotels
}

const hotels = getData();

const validKeys = ['city', 'hotelId', 'room', 'price'];
exports.validKeys = validKeys

/**
 * @param  {object} filter - key, value pair indicating desired value for a key
 * @param  {object} order - sort order. {price: 'asc'} implies sort in ascending order by price
 * @return {array} array of hotels satisfying criteria
 */
exports.getHotels = function (filter, order) {
	const filteredHotels = hotels.filter(function (hotel) {
		for(const k in filter) {
			if(validKeys.indexOf(k) === -1)
				continue;

			const v = filter[k]
			if(hotel[k] !== v) 
				return false;
		}

		return true;
	})

	let sortedHotels = filteredHotels
	let sortOrder;
	

	if(order && Object.keys(order).length > 0) {
		const sortKey = Object.keys(order)[0]; // assumes only one sortKey
		sortOrder = order[sortKey];
		sortedHotels = sortBy(filteredHotels, [sortKey])
	}

	

	if(order && sortOrder === 'desc') {
		return sortedHotels.reverse();
	} else {
		return sortedHotels;
	}
}
