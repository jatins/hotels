/**
 * This module handles all the routes for 'hotels' resource
 */

const log = require('../logger').child({module: 'hotels/index'});
const express = require('express')
const app = express()

const dao = require('./dao')
const validDBKeys = dao.validKeys;

app.get('/', function (req, res) {
	const filter = {}

	for(k in req.query) {
		// skip invalid keys
		if(validDBKeys.indexOf(k) === -1)
			continue;

		filter[k] = req.query[k];
	}

	const {sortKey, sortOrder} = req.query
	let order;
	if(sortKey) {
		order = {}
		order[sortKey] = sortOrder
	}
	
	const hotels = dao.getHotels(filter, order);
	res.send({ok: 1, data: {hotels}})
})


module.exports = app

