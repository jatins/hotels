/**
 * Config parser: reads configurations and converts it to json
 */

var read = require('read-yaml');
const findRoot = require('find-root');
const cwd = require('cwd')
const rootPath = findRoot(cwd())
const configuration = rootPath + '/configuration'

const config = read.sync(configuration + '/rate-limit.yml');

exports.rateLimitConfig = config