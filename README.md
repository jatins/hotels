# Hotel Search
Written in Node.js

## Run
(Requires node installed)

```
$ npm install # Install dependencies
$ npm start   # start server
```

## Tests
```
$ npm run test
```

![Tests Results](http://i.imgur.com/I4JQaMd.png)


## API USAGE
Get all the hotels in particular city 

`GET /hotels?city=Amsterdam&api_key=123`


Sort by Price

`GET /hotels?city=Amsterdam&api_key=123&sortKey=price`


Sort in descreasing order

`GET /hotels?city=Amsterdam&api_key=123&sortKey=price&sortOrder=desc`


Default API rate for an 'api_key' is 0.4 beyond which there is a 5 minute suspend period


## Code
The Rate Limitter is written as an independent module, and the code for that is under `src/middleware/rate-limiter/`


## Logging
Utilizes structured logging


## Configuration
Configuration can be provided by yaml files under `configuration` directory