const cwd = require('cwd')
const findRoot = require('find-root');
const rootPath = findRoot(cwd())
const test = require('tape')
const sinon  = require('sinon');
const src = rootPath + '/src';

const rateLimiter = require(src + '/middleware/rate-limiter')

test('rateLimiter constructor should be a function', function (t) {
	t.plan(1)
	t.equal(typeof rateLimiter, 'function')
})

test('rateLimiter middleware should be a function', function(t) {
	t.plan(1)
	const rates = {}
	const limiter = rateLimiter({key: 'api_key', defaultRate: 1, rates, suspendPeriod: 1})
	t.equal(typeof limiter, 'function')
})

test('rateLimiter middleware should allow requests before limit', function(t) {
	t.plan(1)
	const rates = {}
	const defaultRate = 0.5;
	const suspendPeriod = 1;
	const limiter = rateLimiter({key: 'api_key', defaultRate, rates, suspendPeriod})
	
	const req = {query: {api_key: 12}}
	const next = sinon.spy();
	limiter(req, {}, next)

	t.ok(next.called, "next should be called when rate limit not exceeded")
})

test('rateLimiter middleware should *not* allow requests after limit', function(t) {
	t.plan(2)
	const rates = {}
	const defaultRate = 0.2;
	const suspendPeriod = 1;
	const limiter = rateLimiter({key: 'api_key', defaultRate, rates, suspendPeriod})
	
	const req = {query: {api_key: 12}}
	const next = sinon.spy();
	
	// mock response object
	const res = {}
	res.status = sinon.stub()
	res.status.returns(res)
	res.send = sinon.stub()
	res.send.returns(undefined)

	limiter(req, res, next)
	limiter(req, res, next)
	limiter(req, res, next) // should not be called

	t.ok(next.callCount === 2, "next() should not be called when rate limit exceeded")
	t.ok(res.status.calledWith(403), "response status should be 403 in case of limit exceed")

})