const cwd = require('cwd')
const findRoot = require('find-root');
const rootPath = findRoot(cwd())
const test = require('tape')

const src = rootPath + '/src';

const TokenLimiter = require(src + '/middleware/rate-limiter/TokenLimiter')
const LimitExceededError = require(src + '/middleware/rate-limiter/LimitExceededError')

test('constructor is a function', function (t) {
	t.plan(1)
	t.equal(typeof TokenLimiter, 'function', 'TokenLimiter constructor is a functions')
})

test('throws error on limit exceeded', function (t) {
	t.plan(1)

	// 2 tokens allowed in 10 seconds
	const limiter = new TokenLimiter(2, 10, 1);

	function exceedLimit() {
		limiter.check();
		limiter.check();
		limiter.check();
	}
	
	
	t.throws(exceedLimit, LimitExceededError, "LimitExceeded error should be thrown on limit exceed")	
})

test('no error thrown when limit not exceeded', function (t) {
	t.plan(1)

	// 2 tokens allowed in 10 seconds
	const limiter = new TokenLimiter(5, 10, 1);

	function check() {
		limiter.check();
		limiter.check();
		limiter.check();
	}
	
	
	t.doesNotThrow(check, LimitExceededError, "LimitExceeded error should not thrown")	
})

